<?php

namespace app\api\controller;

use app\common\controller\Api;
use app\common\model\Line;

/**
 * 首页接口
 */
class Index extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];

    /**
     * 首页
     *
     */
    public function index()
    {
        $this->success('请求成功');
    }
    
    /**
     * 会员登录
     *
     * @ApiMethod (POST)
     * @param string $account  账号
     * @param string $password 密码
     */
    public function login()
    {
        $account = $this->request->post('account');
        $password = $this->request->post('password');
        if (!$account || !$password) {
            $this->error(__('Invalid parameters'));
        }
        $ret = $this->auth->login($account, $password);
        if ($ret) {
            $data = ['userinfo' => $this->auth->getUserinfo()];
            // $msg = $data['userinfo']['msg'];
            $data['userinfo']['msg']='好好学习,天天向上!';
            $data['userinfo']['url']='shujiajia.com';
            $data['userinfo']['jstxt']="";
            // array_merge($data['userinfo'],$rr);
            $time=time();
            $this->auth->checkLine($data['userinfo']['id']);
            Line::create(['user_id'=>$data['userinfo']['id'],'update_time'=>$time,'login_time'=>$time]);
            $this->success(__('Logged in successful'), $data);
        } else {
            $this->error($this->auth->getError());
        }
    }
    
    /**
     * @desc统计在线
     *
     */
     public function userTime(){
        //  $user_id=$this->auth->id; 
         $time=time();
         $user_id=1;
         $line_user=Line::where(['user_id'=>$user_id,'is_settle'=>0])->find();
        if($line_user){
            $line_user->save(['update_time'=>$time]);
        }else {
           Line::create(['user_id'=>$user_id,'update_time'=>$time,'login_time'=>$time]);
        }
        $this->success('更新成功',$user_id);
    }
    
    /**
     * @desc扫描在线
     *
     */
     public function userLine(){
         $time=time()-301;
        $line_users=Line::where(['update_time'=>['<',$time],'is_settle'=>0])->select();
        if($line_users){
            foreach ($line_users as $line_user){
                dump($line_user);
                $line_time=$line_user['update_time']-$line_user['login_time'];
                dump($line_time);die('推');
                $line_time=$line_time<0?0:$line_time;
                $line_user->save(['line_time'=>$line_time,'is_settle'=>1]);
            }
        }
        $this->success('扫描成功');
    }
    public function checkLine($user_id){
        $line_users=Line::where(['user_id'=>$user_id,'is_settle'=>0])->select();
        foreach ($line_users as $line_user){
            if($line_user){
                $line_time=$line_user['update_time']-$line_user['login_time'];
                $line_time=$line_time<0?0:$line_time;
                $line_user->save(['is_settle'=>1,'line_time'=>$line_time]);
            }
        }
        return true;
    }
}
