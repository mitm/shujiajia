var regex = /任务/g; // 匹配所有的 "数加加"
var replacement = "测试"; // 替换为 "图萤科技"

// 获取页面上的所有文本节点
var textNodes = document.evaluate("//text()", document, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);

// 遍历文本节点并进行替换
for (var i = 0; i < textNodes.snapshotLength; i++) {
  var textNode = textNodes.snapshotItem(i);
  textNode.nodeValue = textNode.nodeValue.replace(regex, replacement);
}

var divToHide = document.querySelector('.headerBoxOut'); // 将div的classname替换为实际的类名
divToHide.style.display = 'none';

var divToHide = document.querySelector('.footerBg'); // 将div的classname替换为实际的类名
divToHide.style.display = 'none';

//屏蔽所有h2标签
var elements = document.getElementsByTagName("h2");

// 遍历匹配的元素并隐藏它们
for (var i = 0; i < elements.length; i++) {
  elements[i].style.display = "none";
}